import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-temp-template',
  templateUrl: './temp-template.component.html',
})
export class TempTemplateComponent implements OnInit {
  @ViewChild('f', {static: true}) formData;
  radioData = [11, 22, 33, 44];
  optionData = ['hello there', 'hello again'];
  defaultFormValues = {
    // username: 'aa',
    // password: 'bb',
    // email: 'cc@d.e',
    'radio-options': {
      'select-options': 1,
      'radio-group': 33
    }
  };

  constructor() {}

  ngOnInit() {
    this.defaultFormData();
  }

  onSubmit() {
    console.log(this.formData.value);

    this.formData.reset(this.defaultFormValues);
    // this.defaultFormData();
  }

  defaultFormData() {
    // why set timeout?
    // https://stackoverflow.com/questions/50286424/error-there-are-no-form-controls-registered-with-this-group-yet-if-youre-usin
    setTimeout(() => {
      // this.formData.form.setValue({
      //   username: 'rahul',
      //   password: 'abcd',
      //   email: 'a@b.c',
      //   'radio-options': {
      //     'radio-group': 22,
      //     'select-options': 0
      //   }
      // });

      // patchValue won't trigger the error(check the above link)
      this.formData.form.patchValue(this.defaultFormValues);
    });
  }
}
