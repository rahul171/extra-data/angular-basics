import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-temp-reactive',
  templateUrl: './temp-reactive.component.html'
})
export class TempReactiveComponent implements OnInit {
  reactiveForm: FormGroup;
  radioData = [11, 22, 33, 44];
  optionData = ['str 1', 'str 2', 'str 3'];

  constructor() {}

  get somethingControls() {
    return (this.reactiveForm.get('something') as FormArray).controls;
  }

  ngOnInit() {

    this.reactiveForm = new FormGroup({
      username: new FormControl(null, null, this.serverValidator),
      password: new FormControl(null),
      email: new FormControl(null, [Validators.required, Validators.email]),
      radioOptions: new FormControl(null),
      selectOptions: new FormControl(null),
      something: new FormArray([]),
      group: new FormGroup({
        checkbox1: new FormControl(false),
        checkbox2: new FormControl(true)
      })
    });

    // this.reactiveForm.valueChanges.subscribe(value => console.log(value));
    // this.reactiveForm.get('email').statusChanges.subscribe(status => console.log(status));
  }

  serverValidator(control: FormControl): Observable<any> | Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // angular doesn't cancel pending setTimeout
        // (you'll see this log as many times as the outer log)
        console.log('in here deep');
        if (control.value === 'rahul') {
          resolve({invalidServerValidation: true});
        } else {
          resolve(null);
        }
      }, 2000);
      console.log('in here');
    });
  }

  addSomething() {
    const control = new FormControl(null);
    (this.reactiveForm.get('something') as FormArray).push(control);
  }

  onSubmit() {
    console.log(this.reactiveForm.value);
    // console.log(this.reactiveForm.get('group.checkbox1'));
    // // or
    // console.log(this.reactiveForm.get('group').get('checkbox1'));
  }
}
