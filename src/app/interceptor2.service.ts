import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

export class Interceptor2Service implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    const newReq = req.clone({
      headers: req.headers.append('custom-2', 'boom boom 2')
    });

    console.log('in 2');

    return next.handle(newReq);
  }
}
