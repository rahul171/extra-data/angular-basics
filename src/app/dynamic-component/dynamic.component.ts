import {AfterViewInit, Component, ComponentFactoryResolver, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {DynamicChildComponent} from './dynamic-child/dynamic-child.component';
import {PlaceholderDirective} from './placeholder.directive';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html'
})
export class DynamicComponent implements OnInit {
  showPopup = false;
  @ViewChild('popup1', {read: PlaceholderDirective, static: false}) popupPlaceHolder1: PlaceholderDirective;
  @ViewChild('popup2', {read: PlaceholderDirective, static: false}) popupPlaceHolder2: PlaceholderDirective;

  constructor(private cfResolver: ComponentFactoryResolver) {}

  ngOnInit() {}

  triggerngIfPopup() {
    this.showPopup = !this.showPopup;
  }

  triggerDynamicPopup() {

    const dccFactory = this.cfResolver.resolveComponentFactory(DynamicChildComponent);
    const vcRef = this.popupPlaceHolder2.vcRef;

    vcRef.clear();

    const dccComponent = vcRef.createComponent(dccFactory);

    dccComponent.instance.closeEvent.subscribe(() => {
      vcRef.clear();
    });
  }
}
