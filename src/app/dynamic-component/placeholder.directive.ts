import {Directive, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appPlaceholder]'
})
export class PlaceholderDirective {
  a = 0;

  constructor(public vcRef: ViewContainerRef) {}

  getValue() {
    return ++(this.a);
  }
}
