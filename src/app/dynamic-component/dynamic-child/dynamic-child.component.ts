import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-dynamic-child',
  templateUrl: './dynamic-child.component.html',
  styleUrls: ['./dynamic-child.component.css']
})
export class DynamicChildComponent implements OnInit {
  @Output('close') closeEvent = new EventEmitter<void>();

  constructor() {}

  ngOnInit() {}

  closePopup() {
    this.closeEvent.emit();
    console.log('DynamicChildComponent -> closePopup()');
  }

  stopPropogate(event) {
    event.stopPropagation();
    console.log('DynamicChildComponent -> stopPropogate()');
  }
}
