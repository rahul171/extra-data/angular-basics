import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString: string) {

    if (!filterString) {
      return value;
    }

    const arr = [];

    return value.filter((item) => {
      return item.includes(filterString);
    });
  }
}

