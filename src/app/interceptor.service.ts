import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {tap} from 'rxjs/operators';

export class InterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // const newReq = req.clone({
    //   headers: req.headers.append('hello', 'there')
    // });
    //
    // console.log('req');
    // console.log(req);
    //
    // return next.handle(newReq)
    //   .pipe(
    //     tap((data) => {
    //       console.log('piped data');
    //       console.log(data);
    //       data.abcd = 'hello';
    //       return data;
    //     })
    //   );

    const newReq = req.clone({
      headers: req.headers.append('custom-1', 'boom boom 1')
    });

    console.log('in 1');

    return next.handle(newReq);
  }
}
