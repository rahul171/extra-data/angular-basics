import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TempTemplateComponent} from './forms/temp-template/temp-template.component';
import {TempComponent} from './temp/temp.component';
import {Temp3Component} from './temp/temp3/temp3.component';
import {Temp4Component} from './temp/temp4/temp4.component';
import {Temp2Component} from './temp2/temp2.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {AuthGuardService} from './guards/auth-guard.service';
import {AuthGuardDeactivateService} from './guards/auth-guard-deactivate.service';
import {ResolveService} from './guards/resolve.service';
import {TempReactiveComponent} from './forms/temp-reactive/temp-reactive.component';
import {FormsComponent} from './forms/forms.component';
import {TempDefaultComponent} from './temp-default/temp-default.component';
import {PipesComponent} from './pipes-component/pipes.component';
import {HttpComponent} from './http/http.component';
import {DynamicComponent} from './dynamic-component/dynamic.component';

const routes: Routes = [
  {
    path: '',
    component: TempDefaultComponent
  },
  {
    path: 'dynamic',
    component: DynamicComponent
  },
  {
    path: 'http',
    component: HttpComponent
  },
  {
    path: 'pipes',
    component: PipesComponent
  },
  {
    path: 'forms',
    component: FormsComponent,
    children: [
      {
        path: 'template',
        component: TempTemplateComponent
      },
      {
        path: 'reactive',
        component: TempReactiveComponent
      }
    ]
  },
  {
    path: 'temp2',
    component: Temp2Component
  },
  {
    path: 'temp',
    component: TempComponent,
    // resolve: {tData: ResolveService},
    // canActivate: [AuthGuardService],
    // canActivateChild: [AuthGuardService],
    // canDeactivate: [AuthGuardDeactivateService],
    children: [
      {
        path: 'temp3',
        component: Temp3Component
      },
      {
        path: 'temp4',
        component: Temp4Component
      },
      // {
      //   path: 'temp4/:id',
      //   component: Temp4Component
      // },
      // for ** in children, you must include path: '' otherwise ** wildcard will catch parent / path (/temp)
      // {
      //   path: '',
      //   component: TempComponent,
      // },
      // {
      //   path: '**',
      //   redirectTo: '/temp2'
      // }
    ]
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/not-found'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
