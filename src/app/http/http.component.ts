import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {catchError, exhaustMap, map, take, tap} from 'rxjs/operators';
import {BehaviorSubject, Subject, throwError} from 'rxjs';

@Component({
  selector: 'app-http',
  templateUrl: './http.component.html'
})
export class HttpComponent implements OnInit {
  url = 'http://localhost:4000/';

  tempSub = new Subject();
  tempBehaviourSub = new BehaviorSubject('bhai bhai');

  constructor(private http: HttpClient) {}

  ngOnInit() {}

  sendGetExhaustMap() {

    this.http.get(this.url)
      .pipe(
        exhaustMap(data => {
          return this.tempSub.pipe(
            map(anotherData => {
              return anotherData + '_' + data[0].name;
            })
          );
        })
      ).subscribe(data => {
        console.log('exhaust get/tempSub data');
        console.log(data);
      });
  }

  getGetExhaustMap() {
    this.tempSub.next('prefix');
  }

  setBs() {

    // this.http.get(this.url)
    //   .pipe(
    //     map(data => {
    //       console.log('inside map');
    //       console.log(data);
    //       return !!data;
    //     })
    //   )
    //   .subscribe(data => {
    //     console.log('test');
    //     console.log(data);
    //   });

    this.tempBehaviourSub.pipe(
      take(2) // this subscription will be valid for {count}(in this case 2) values, then it will unsubscribe
    ).subscribe(data => {
      console.log('behaviour sub');
      console.log(data);
    });
  }

  bs1() {
    this.tempBehaviourSub.next('rahul1');
  }

  bs2() {
    this.tempBehaviourSub.next('rahul2');
  }

  bs3() {
    this.tempBehaviourSub.next('rahul3');
  }

  bs4() {
    this.tempBehaviourSub.next('rahul4');
  }

  sendGet() {
    // let params = new HttpParams();
    // params = params.append('action', 'get');

    this.http.get(this.url).subscribe(data => {
        console.log('get data');
        console.log(data);
      });
  }

  sendPost() {
    this.http.post(this.url, {
      id: 33,
      name: 'rahul'
    }, {
      observe: 'response'
    }).subscribe(data => {
      console.log('post data');
      console.log(data);
    });
  }

  sendDelete() {

    this.http.delete(this.url).subscribe(data => {
      console.log('delete data');
      console.log(data);
    }, error => {
      console.log(error);
    });
  }

  getError() {

    this.http.get(this.url + 'error').pipe(
      catchError((errorRes) => {
        // return throwError('custom error message');
        return throwError(errorRes);
      })
    ).subscribe(data => {
      console.log('data');
      console.log(data);
    }, error => {
      console.log('error');
      console.log(error);
    });
  }

  clearConsole() {
    console.clear();
  }
}
