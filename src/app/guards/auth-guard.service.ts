import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {AuthService} from '../auth.service';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.authService.login();

    console.log('AuthGuardService -> canActivate()');

    return this.authService.isAuthenticated() ? true : (() => {
        this.router.navigate(['temp2'], {relativeTo: this.route});
        return false;
      })();
  }

  canActivateChild(route: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    console.log('AuthGuardService -> canActivateChild()');

    return this.canActivate(route, state);
  }
}
