import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

interface TempData {
  id: number;
  name: string;
}

export class ResolveService implements Resolve<TempData> {

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TempData> | Promise<TempData> | TempData {

    console.log('ResolveService -> resolve()');

    return { id: 22, name: 'test' };

    // return new Promise<TempData>((resolve, reject) => {
    //   setTimeout(() => {
    //     resolve({ id: 22, name: 'test' });
    //   }, 2000);
    // }).then((value) => {
    //   value.id = 444;
    //   return value;
    // }).then((value) => {
    //   value.name = 'rahul';
    //   return value;
    // });
  }
}
