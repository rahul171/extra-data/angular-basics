import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {CustomCanDeactivate} from '../interfaces/custom-can-deactivate';


export class AuthGuardDeactivateService implements CanDeactivate<CustomCanDeactivate> {

  canDeactivate(component: CustomCanDeactivate,
                route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot,
                nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    console.log('AuthGuardDeactivateService -> canDeactivate()');

    return component.customCanDeactivate();
  }
}
