import {Component, OnDestroy, OnInit} from '@angular/core';
import {CustomCanDeactivate} from '../interfaces/custom-can-deactivate';
import {ActivatedRoute} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {filter, map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-temp',
  templateUrl: './temp.component.html'
})
export class TempComponent implements OnInit, CustomCanDeactivate, OnDestroy {
  customObservableSubscription: Subscription;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {

    // this.route.data.subscribe((data) => {
    //   console.log('data');
    //   console.log(data);
    // });

    // let customObservable = new Observable((observer) => {
    //   let count = 0;
    //   setInterval(() => {
    //     // if (count === 2) { observer.error(new Error('test error')); }
    //     if (count === 15) { observer.complete(); }
    //     observer.next(++count);
    //   }, 1000);
    // });
    //
    // customObservable = customObservable.pipe(
    //   tap(() => { console.log('...'); }),
    //   filter((item) => item < 5 || item > 10),
    //   map((item) => +item)
    // );
    //
    // this.customObservableSubscription = customObservable.subscribe((count) => {
    //   console.log(count);
    // }, (error) => {
    //   console.log('error');
    //   console.log(error);
    // }, () => {
    //   console.log('completed');
    // });

  }

  ngOnDestroy() {
    // this.customObservableSubscription.unsubscribe();
  }

  customCanDeactivate() {
    console.log('TempComponent -> customCanDeactivate()');
    return true;
  }
}
