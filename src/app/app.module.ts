import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { TempComponent } from './temp/temp.component';
import { Temp3Component } from './temp/temp3/temp3.component';
import { Temp4Component } from './temp/temp4/temp4.component';
import { Temp2Component } from './temp2/temp2.component';
import { HeaderComponent } from './header/header.component';
import { TempTemplateComponent } from './forms/temp-template/temp-template.component';

import { AppRoutingModule } from './app-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';
import {AuthGuardService} from './guards/auth-guard.service';
import {AuthService} from './auth.service';
import {AuthGuardDeactivateService} from './guards/auth-guard-deactivate.service';
import {ResolveService} from './guards/resolve.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TempReactiveComponent} from './forms/temp-reactive/temp-reactive.component';
import {FormsComponent} from './forms/forms.component';
import { TempDefaultComponent } from './temp-default/temp-default.component';
import { PipesComponent } from './pipes-component/pipes.component';
import {FilterPipe} from './filter.pipe';
import {HttpComponent} from './http/http.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {InterceptorService} from './interceptor.service';
import {Interceptor2Service} from './interceptor2.service';
import {DynamicComponent} from './dynamic-component/dynamic.component';
import {DynamicChildComponent} from './dynamic-component/dynamic-child/dynamic-child.component';
import {PlaceholderDirective} from './dynamic-component/placeholder.directive';

@NgModule({
  declarations: [
    AppComponent,
    TempComponent,
    Temp2Component,
    Temp3Component,
    Temp4Component,
    HeaderComponent,
    FormsComponent,
    TempTemplateComponent,
    TempReactiveComponent,
    NotFoundComponent,
    TempDefaultComponent,
    PipesComponent,
    FilterPipe,
    HttpComponent,
    DynamicComponent,
    DynamicChildComponent,
    PlaceholderDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthGuardService, AuthService, AuthGuardDeactivateService, ResolveService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor2Service,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [DynamicChildComponent]
})
export class AppModule { }
