import {Observable} from 'rxjs';

export interface CustomCanDeactivate {

  customCanDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}
